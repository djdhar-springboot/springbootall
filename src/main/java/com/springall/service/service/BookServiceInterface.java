package com.springall.service.service;

import com.springall.service.model.Book;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

public interface BookServiceInterface {

    Mono<Book> getBook(Long id);

    Mono<Book> createBook(Book book);

    Mono<Book> updateBook(Book book);

    Mono<Void> deleteBook(Long id);
}
