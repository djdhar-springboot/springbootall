package com.springall.service.service;

import com.springall.service.model.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import org.springframework.stereotype.Service;

@Service
public class ReactiveProducerService {

    private final Logger log = LoggerFactory.getLogger(ReactiveProducerService.class);
    private final ReactiveKafkaProducerTemplate<String, Book> reactiveKafkaProducerTemplate;

    @Value(value = "${FAKE_PRODUCER_DTO_TOPIC}")
    private String topic;

    public ReactiveProducerService(ReactiveKafkaProducerTemplate<String, Book> reactiveKafkaProducerTemplate) {
        this.reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate;
    }

    public void send(Book Book) {
        log.info("send to topic={}, {}={},", topic, Book.class.getSimpleName(), Book);
        reactiveKafkaProducerTemplate.send(topic, Book)
                .doOnSuccess(senderResult -> log.info("sent {} offset : {}", Book, senderResult.recordMetadata().offset()))
                .subscribe();
    }
}
