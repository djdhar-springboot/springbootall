package com.springall.service.service;

import com.springall.service.client.ReactiveRedisClient;
import com.springall.service.model.Book;
import com.springall.service.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Objects;

@Service
public class BookService implements BookServiceInterface {

    @Autowired
    ReactiveRedisClient redisClient;

    @Autowired
    BookRepository bookRepository;

    @Autowired
    ReactiveProducerService reactiveProducerService;
    public Mono<Book> createBook(Book book) {
        return bookRepository.save(book).map(createdBook -> {
            redisClient.put(createdBook.getBookId().toString(), createdBook).subscribe();
            return createdBook;
        }).doOnNext(createdBook -> {
            reactiveProducerService.send(createdBook);
        });
    }

    public Mono<Book> updateBook(Book book) {
        return bookRepository.findByBookId(book.getBookId()).flatMap(bookToUpdate -> {
            book.setId(bookToUpdate.getId());
            return bookRepository.save(book).map(updatedBook -> {
                redisClient.put(updatedBook.getBookId().toString(), updatedBook).subscribe();
                return updatedBook;
            }).doOnNext(updatedBook -> {
                reactiveProducerService.send(updatedBook);
            });
        });
    }

    public Mono<Void> deleteBook(Long id) {
        return redisClient.remove(id.toString()).flatMap(removed -> {
            return bookRepository.deleteByBookId(id).then();
        });
    }

    public Mono<Book> getBook(Long id) {
        return redisClient.get(id.toString()).map(book -> {
                    if(Objects.equals(book, new Book())) throw new RuntimeException();
                    else return book;
                }).switchIfEmpty(Mono.defer(() -> {
                        return bookRepository.findByBookId(id);
                })).onErrorResume(throwable -> {
                        return bookRepository.findByBookId(id);
                });
    }
}
