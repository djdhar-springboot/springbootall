package com.springall.service.controller;

import com.springall.service.model.Book;
import com.springall.service.service.BookService;
import com.springall.service.service.BookServiceInterface;
import com.springall.service.service.ReactiveConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.Date;

@RestController
public class BookController {

    @Autowired
    BookServiceInterface bookServiceInterface;

    @GetMapping("v1/getBook/{id}")
    public Mono<Book> getBook(@PathVariable Long id) {
        return bookServiceInterface.getBook(id);
    }

    @PostMapping("v1/createBook")
    public Mono<Book> createBook(@RequestBody Book book) {
        return bookServiceInterface.createBook(book);
    }

    @PutMapping("v1/updateBook")
    public Mono<Book> updateBook(@RequestBody Book book) {
        return bookServiceInterface.updateBook(book);
    }

    @DeleteMapping("v1/deleteBook/{id}")
    public Mono<Void> deleteBook(@PathVariable Long id) {
        return bookServiceInterface.deleteBook(id);
    }

}
