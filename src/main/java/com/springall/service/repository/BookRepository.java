package com.springall.service.repository;

import com.springall.service.model.Book;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface BookRepository extends ReactiveMongoRepository<Book, String> {

    public Mono<Book> findByBookId(Long bookId);
    public Mono<Void> deleteByBookId(Long bookId);

}
