package com.springall.service;

import com.springall.service.service.ReactiveConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.springall.service"})
public class ServiceApplication {

	@Autowired
	ReactiveConsumerService reactiveConsumerService;

	public static void main(String[] args) {
		SpringApplication.run(ServiceApplication.class, args);
	}

}
