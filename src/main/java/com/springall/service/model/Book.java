package com.springall.service.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "book")
public class Book {

    @JsonIgnore
    @Id
    String id;
    @Indexed(unique=true)
    Long bookId;
    String bookName;
    String bookAuthor;
    Date publishDate;
    Long purchaseAmount;
}
