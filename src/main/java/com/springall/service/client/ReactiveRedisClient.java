package com.springall.service.client;

import com.google.gson.Gson;
import com.springall.service.model.Book;
import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.reactive.RedisStringReactiveCommands;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class ReactiveRedisClient {
    RedisClient client = RedisClient.create("redis://localhost:6379");
    StatefulRedisConnection<String, String> connection = client.connect();
    RedisStringReactiveCommands<String, String> reactive = connection.reactive();

    public Mono<Book> get(String key) {
        return reactive.get(key)
                .map(value -> new Gson().fromJson(value, Book.class));
    }

    public Mono<Book> put(String key, Book book) {
        String value = new Gson().toJson(book);
        return reactive.set(key, value).map(res -> book);
    }

    public Mono<String> remove(String id) {
        return reactive.set(id, "{}");
    }
}
